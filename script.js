function filterBy(arr, dataType) {
    return arr.filter(item => typeof item !== dataType);
}

const inputArray = ['hello', 'world', 23, '23', null, 23, 55];
const dataType = 'string';
const filteredArray = filterBy(inputArray, dataType);

console.log(filteredArray); 